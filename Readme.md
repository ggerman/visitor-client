# VISITOR CLIENT

## sendBeacon:

The navigator.sendBeacon() method can be used to asynchronously transfer small HTTP data from the User Agent to a web server.

```
navigator.sendBeacon(url, data);
```

```
window.addEventListener('unload', logData, false);

function logData() {
    navigator.sendBeacon("/log", analyticsData);
}
```

## javaEnabled:

The javaEnabled() method returns a Boolean value that specifies whether the browser has Java enabled.

```
var x = "Java Enabled: " + navigator.javaEnabled();
```

## getGamepads:

The Navigator.getGamepads() method returns an array: the first value is null, and the others are all Gamepad objects, one for each gamepad connected to the device. So if no gamepads are connected, the method will just return null.

```
var arrayGP = navigator.getGamepads();
```

### Example

```
window.addEventListener("gamepadconnected", function(e) {
  var gp = navigator.getGamepads()[0];
  console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
  gp.index, gp.id,
  gp.buttons.length, gp.axes.length);
});
```

## requestMediaKeySystemAccess:

The Navigator.requestMediaKeySystemAccess() method returns a Promise for a MediaKeySystemAccess object.

```
Navigator.requestMediaKeySystemAccess(keySystem, supportedConfigurations).then(function(mediaKeySystemAccess) { ... });
```

## registerProtocolHandler:

Allows web sites to register themselves as possible handlers for particular protocols.

For security reasons, by default, web sites may only register protocol handlers for themselves — the domain and protocol of the handler must match the current site. However, users may set a preference in Firefox to allow cross website installation, by setting the gecko.handlerService.allowRegisterFromDifferentHost pref to true in about:config.

Extensions can register protocol handlers targeting other sites: see the 'See Also' section for how to use them from XPCOM.

### Syntax

```
window.navigator.registerProtocolHandler(protocol, uri, title);
```

```
navigator.registerProtocolHandler("burger",
                                  "https://www.google.co.uk/?uri=%s",
                                  "Burger handler");
```                                  


## registerContentHandler:

Allows web sites to register themselves as possible handlers for content of a particular MIME type.

```
navigator.registerContentHandler(mimeType, uri, title);
```

```
navigator.registerContentHandler(
    "application/vnd.mozilla.maybe.feed",
    "http://www.example.tld/?foo=%s",
    "My Feed Reader"
);
```

## taintEnabled:

Determine whether your browser has data tainting enabled:

```
document.write("Data tainting enabled: " + navigator.taintEnabled());
```

## mimeTypes:

Returns a MimeTypeArray object, which contains a list of MimeType objects representing the MIME types recognized by the browser.

### Syntax

```
mimeTypes = navigator.mimeTypes;
```

```
function isJavaPresent() {
  return 'application/x-java-applet' in navigator.mimeTypes;
}

function getJavaPluginDescription() {
  var mimetype = navigator.mimeTypes['application/x-java-applet'];
  if (mimetype === undefined) {
    // no Java plugin present
    return undefined;
  }
  return mimetype.enabledPlugin.description;
}
```

