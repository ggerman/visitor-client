###############################################
# structure for save information from client.js
# author: ggerman@gmail.com
# date: 30 Oct 2015
# code: CoffeeScript
#

out = { 
  version: "0.1a"
  author: {
    name: "Gimenez Silva German Alberto"
    email: "ggerman@gmail.com"
  }

  ip:"0.0.0.0" 

  battery: { 
    charging: 0
    level: 0
    chargingTime: 0
    dischargingTime: 0 
  }

  geo: {
    latitude: 0
    longitude: 0
    accuracy: 0
    altitude: 0
    altitudeAccuracy: 0
    heading: 0
    speed: 0 
  }

  appCodeName: ""
  appName: ""
  appName: ""
  appVersion: ""
  cookieEnabled: ""
  onLine: ""
  platform: ""
  product: ""
  userAgent: ""
  maxTouchPoints: "empty"
  url: ""
  referrer: ""
  }

me = "yo yo"


